#!/usr/bin/python
#import requests
#import re
import pygal
import pickle
from pygal import Config
from pygal.style import *
#from time import sleep

#config for graphs
config = Config()
config.show_legend = True
config.human_readable = True
config.x_label_rotation=85
config.x_show_minor_labels = False
config.style = DarkStyle
config.width = 2000
config.height = 1500
config.print_zeroes = False

events = []
with open('list2','r') as f:
	for line in f:
		events.append(line.strip())

#basic chart setup
chart = pygal.Line(config)
chart.title = "Historic Donation Tracker"

#print events
for event in events:
	#print "{}".format(event[:-3])
	filename = "{}.json".format(event)
	#print filename
	with open(filename, 'rb') as j:
		dt = pickle.load(j)
	#print "length of DT: {}".format(len(dt))
	#22 seems to be a the perfect number for all sets...
	mod_num = 100
	i = 0
	c = 0
	ds = []
	for x in dt:
		if i%mod_num == 0:
			c = c+1
			ds.append(x)
		i = i+1
	ds.append(x)
	chart.add(event, ds)

print "Done... Rendering File.."
chart.render_to_file('/root/nope/histchart.svg')

