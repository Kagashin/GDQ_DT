#!/usr/bin/python
import requests
import re
import pygal
import pickle
from pygal import Config
from pygal.style import *
from time import sleep

#config for graphs
config = Config()
config.show_legend = True
config.human_readable = True
config.x_label_rotation=85
config.x_show_minor_labels = False
config.style = DarkStyle
config.width = 2000
config.height = 1500
config.print_zeroes = False

#setup basic vars
events = []
page = 1


#get list of all events
with open('list') as f:
	for line in f:
		events.append(line.strip())


#regex strings
rpage='(<label for="sort">of )(\d+)'
regstr = '(<tr class="">\n.+\n.+\n.+\n.+\n<span class="datetime">)(\d{2}/\d{2}/\d{4} \d{2}:\d{2}:\d{2})(.+\n.+\n.+\n)(<a href="/tracker/donation/\d+">\$)([\d\.\,]+)'
chart = pygal.Line(config)
chart.title = "Historic Donation Tracker"
print "We will be working on {} events".format(len(events))
for event in events:
        #base_url="https://gamesdonequick.com/tracker/donations/{}?page={}".format(event,page)
        print "================={}==============".format(event)


	print "Making first request"
	r = requests.get('https://gamesdonequick.com/tracker/donations/{}?page=1'.format(event), timeout=300)
	if r.status_code != 200:
		print "we got an error... Status Code: {}".format(r.status_code)
		exit()
	else:
		print "we are good"
	page = r.text
	m=re.search(rpage,r.text)

	if m:
		totalpages = int(m.group(2))
		print "Total pages found: {}".format(totalpages)
	else:
		print "regex error..."
		exit()
	i=0
        try:
		print "trying to load up prev"
                with open('h','rb') as h:
			ev = pickle.load(h)
			print ev
			if ev == event:
	                	start = pickle.load(h)
				print start
        	                ds = pickle.load(h)
				print ds[-1]
				dtotal = ds[-1]
				dt = pickle.load(h)
				print dt[-1]
			else:
				dt = []
				ds = []
				start = 1
				dtotal = 0.0
        except:
        	print "didnt work..."
		ds = []
		dt = []
		start = 1
		print start
		dtotal = 0.0
	print start
	for x in range(start,(totalpages+1)):
		print "working on page: {}, total so far {}".format(x,dtotal)
		with requests.Session() as s:
			sleep(2)
			r = s.get('https://gamesdonequick.com/tracker/donations/{}?page={}&sort=time&order=1'.format(event,x), timeout=300)
			if r.status_code != 200:
	        		print "we got an error... Status Code: {}".format(r.status_code)
		        	exit()
			m = re.findall(regstr, r.text)
			for a in m:
				#print a[4]
				dtotal += float(a[4].replace(",",""))
				ds.append(dtotal)
				dt.append(a[1])
				#if (i%75 == 0):
				#	ds.append(dtotal)
				#	dt.append(a[1])
				#else:
				#	ds.append(None)
				#	dt.append(None)
				#i=i+1
			if x%10 == 0:
				print "CHECKPOINT!"
				with open('h','wb') as h:
					pickle.dump(event,h)
					pickle.dump(x+1,h)
					pickle.dump(ds,h)
					pickle.dump(dt,h)
	print "End total: {}, over {} donations".format(dtotal, i)
	chart.add(event, ds)
	with open('{}-ds.json'.format(event),'w') as out:
		pickle.dump(ds,out)
	with open('{}-dt.json'.format(event),'w') as out:
		pickle.dump(dt,out)

print "Done... rendering file"
chart.render_to_file('/root/nope/histchart.svg')

