#!/usr/bin/python
#imports
import requests
import re
import pygal
import operator
import ConfigParser
import ast
from pygal import Config
from pygal.style import *

#config for graphs
config = Config()
config.show_legend = True
config.human_readable = True
config.x_label_rotation=85
config.x_show_minor_labels = False
config.style = DarkStyle
config.width = 2000
config.height = 1000
config.print_zeroes = False


#getting to be too many pages... configparser
start = 1
dtotal = 0.0
br = {}

rpage='(<label for="sort">of )(\d+)'
regstr = '(<tr class="">\n.+\n.+\n.+\n.+\n<span class="datetime">)(\d{2}/\d{2}/\d{4} \d{2}:\d{2}:\d{2})(.+\n.+\n.+\n)(<a href="/tracker/donation/\d+">\$)([\d\.\,]+)'

print "Making first request"
r = requests.get('https://gamesdonequick.com/tracker/donations/sgdq2017?page=1')
if r.status_code != 200:
	print "we got an error... Status Code: {}".format(r.status_code)
	exit()
else:
	print "we are good"
page = r.text
m=re.search(rpage,r.text)
if m:
	totalpages = int(m.group(2))
	print "Total pages found: {}".format(totalpages)
	print "We will save up till page {}".format(totalpages-1)
else:
	print "regex error..."
	exit()
ds = []
i=0
ds.append(dtotal)
dt = []

for x in range(start,(totalpages+1)):
	print "working on page: {}, total so far {}".format(x,dtotal)
	with requests.Session() as s:
		r = s.get('https://gamesdonequick.com/tracker/donations/sgdq2017?page={}&sort=time&order=1'.format(x))
		if r.status_code != 200:
        		print "we got an error... Status Code: {}".format(r.status_code)
	        	exit()
		m = re.findall(regstr, r.text)
		#print len(m)
		for a in m:
			#print a[4]
			dtotal += float(a[4].replace(",",""))
			if (i%75 == 0):
				ds.append(dtotal)
				dt.append(a[1])
			else:
				ds.append(None)
				dt.append(None)
			i=i+1
			#print a[1]
			if a[1][:13] not in br:
				br[a[1][:13]] = float(a[4].replace(",",""))
			else:
				br[a[1][:13]] = br[a[1][:13]]  + float(a[4].replace(",",""))
	if x == totalpages-1:
		print "We have hit the save page!"
print "End total: {}, over {} donations".format(dtotal, i)

chart = pygal.Line(config)
chart.title = "Donation Tracker"
chart.x_labels = dt
chart.add('Donations', ds)
chart.render_to_file('/root/nope/chart.svg')
sorted_x = sorted(br.items(), key=operator.itemgetter(0))
bars = pygal.Bar(config)
for x in sorted_x:
	if x[1] > 999:
		bars.add(x[0],x[1], rounded_bars=8)
bars.render_to_file('/root/nope/bars.svg')
